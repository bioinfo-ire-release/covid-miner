#!/bin/bash

grep Ser477Asn ./covid-miner/results/gisaid_20210117/snps/*vcf > ./covid-miner/results/gisaid_20210117/rbd/test.txt
cut -f 9 ./covid-miner/results/gisaid_20210117/rbd/test.txt  > ./covid-miner/results/gisaid_20210117/rbd/seq_name_S477N.txt
rm ./covid-miner/results/gisaid_20210117/rbd/test.txt

grep Asn439Lys ./covid-miner/results/gisaid_20210117/snps/*vcf > ./covid-miner/results/gisaid_20210117/rbd/test.txt
cut -f 9 ./covid-miner/results/gisaid_20210117/rbd/test.txt  > ./covid-miner/results/gisaid_20210117/rbd/seq_name_N439K.txt
rm ./covid-miner/results/gisaid_20210117/rbd/test.txt

grep Asn501Tyr ./covid-miner/results/gisaid_20210117/snps/*vcf > ./covid-miner/results/gisaid_20210117/rbd/test.txt
cut -f 9 ./covid-miner/results/gisaid_20210117/rbd/test.txt  > ./covid-miner/results/gisaid_20210117/rbd/seq_name_N501Y.txt
rm ./covid-miner/results/gisaid_20210117/rbd/test.txt
