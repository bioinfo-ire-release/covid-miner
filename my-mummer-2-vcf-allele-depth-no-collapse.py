#!/usr/bin/env python3
# adaptation from the original script from Matteo Schiavinato https://github.com/MatteoSchiavinato/Utilities

import argparse
import sys
from time import strftime
from Bio import SeqIO
from operator import itemgetter
from pandas import DataFrame

if len(sys.argv) <= 1:
    sys.argv.append("-h")

if sys.argv[1] in ["-h", "--help", "getopt", "usage", "-help", "help"]:
    sys.exit('''
USAGE:  my-mummer-2-vcf.py [ options ]
Convert Mummer SNP/indel output as produced by the 'show-snps -T' command to
a pseudo-VCF format.
OPTIONS:
    -s|--snps		    	Mummer "snps" file (output of 'show-snps -T')
                (mandatory)
    -g|--reference		Reference genome FASTA file for header generation
                (mandatory)
       --input-header		Use this flag if the file has a header
    -n|--no-Ns		    	Exclude variants featuring Ns
                (default: off)
    -t|--type   SNP|INDEL|ALL   Restrict the output to just SNPs or indels
                (default: ALL)
       --output-header		Add a newly-generated VCF header to the output
''')

# parser
p = argparse.ArgumentParser()
p.add_argument("-s", "--snps", required=True)
p.add_argument("-g", "--reference", required=True)
p.add_argument("--input-header", action="store_true")
p.add_argument("-n", "--no-Ns", action="store_true")
p.add_argument("-t", "--type", choices=["SNP", "INDEL", "ALL"], default="ALL")
p.add_argument("--output-header", action="store_true")
args = p.parse_args()

### conditions ###

if (args.output_header) and not args.reference:
    sys.exit("ERROR: --add-vcf-header requires --reference as well\n\n")


# functions

def remove_header(Lines):
    Lines_no_header = Lines[4:]
    return Lines_no_header


def convert_snps_to_vcf(line):
    # CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO
    lst = line.rstrip("\n\r\b").split("\t")
    scaffold = lst[10]
    pos = lst[0]
    ID = "."
    ref = lst[1]
    alt = lst[2]
    qual = "."
    filt = "."
    var_type = "."
    orig_pos = str(lst[11]) + ":" + str(lst[3])
    vcf_line = "\t".join([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])
    return vcf_line


def exclude_Ns(Vcf_lines):
    Vcf_lines_no_Ns = []
    for line in Vcf_lines:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")

        if (("N" not in ref) and ("n" not in ref) and \
                ("N" not in alt) and ("n" not in alt)):
            Vcf_lines_no_Ns.append("\t".join((scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos)))

    return Vcf_lines_no_Ns


def infer_var_type(Vcf_lines):
    Vcf_lines_w_types = []
    for line in Vcf_lines:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")

        if ((len(ref) == 1) and ("." not in ref) and \
                (len(alt) == 1) and ("." not in alt)):
            var_type = "."

        else:
            var_type = "INDEL"

        Vcf_lines_w_types.append("\t".join((scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos)))

    return Vcf_lines_w_types


def count_sequences(Vcf_lines):
    sequences = []
    for line in Vcf_lines:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        seq_per_var_lst = orig_pos.split(",")
        for seq in seq_per_var_lst:
            seq_id = seq.split(":")[0]
            if seq_id not in sequences:
                sequences.append(seq_id)
    n_sequences = len(sequences)

    return n_sequences


def collapse_snps(Sorted_snps, n_sequences):
    Collapsed_snps = []
    d = dict()
    for i in range(len(Sorted_snps)):
        line = Sorted_snps[i]
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")

        if len(Collapsed_snps) == 0:
            Collapsed_snps.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])
            d.setdefault(alt, []).append(orig_pos)

        elif int(pos) == pos_old and orig_pos != orig_pos_old:
            d.setdefault(alt, []).append(orig_pos)
            if i == len(Sorted_snps) - 1:
                # last line
                sorted_items = sorted(d.items(), key=lambda item: len(item[1]), reverse=True)
                d_sorted = dict(sorted_items)

                alt_allele_counts = []
                for key, value in d_sorted.items():
                    alt_allele_counts.append(len(value))
                for index, (key, value) in enumerate(d_sorted.items()):
                    allele_depth = "AD=" + ",".join([str(n_sequences - sum(alt_allele_counts)), str(len(value))])
                    if index == 0:
                        Collapsed_snps[-1][7] = allele_depth
                        Collapsed_snps[-1][4] = key
                        Collapsed_snps[-1][8] = ",".join(value)
                    else:
                        Collapsed_snps.append(
                            [scaffold, str(pos_old), ID, ref, key, qual, filt, allele_depth, ",".join(value)])

        elif int(pos) == pos_old and orig_pos == orig_pos_old:
            #duplicate snp record
            continue

        else:
            sorted_items = sorted(d.items(), key=lambda item: len(item[1]), reverse=True)
            d_sorted = dict(sorted_items)
            alt_allele_counts = []
            for key, value in d_sorted.items():
                alt_allele_counts.append(len(value))
            for index, (key, value) in enumerate(d_sorted.items()):
                allele_depth = "AD=" + ",".join([str(n_sequences - sum(alt_allele_counts)), str(len(value))])
                if index == 0:
                    Collapsed_snps[-1][7] = allele_depth
                    Collapsed_snps[-1][4] = key
                    Collapsed_snps[-1][8] = ",".join(value)
                else:
                    Collapsed_snps.append(
                        [scaffold, str(pos_old), ID, ref_old, key, qual, filt, allele_depth, ",".join(value)])

            d = dict()

            d.setdefault(alt, []).append(orig_pos)
            Collapsed_snps.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])

        pos_old = int(pos)
        ref_old = ref
        orig_pos_old = orig_pos

    return Collapsed_snps


def collapse_insertions(Insertions, n_sequences):
    Collapsed_insertions = []
    Tmp_insertions = []
    d = dict()
    flag = 0
    allele_depth_base = "AD=" + ",".join([str(n_sequences - 1), str(1)])
    for i in range(len(Insertions)):
        line = Insertions[i]
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        id = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos.split("|")))

        if len(Collapsed_insertions) == 0:
            Collapsed_insertions.append([scaffold, pos, ID, ref, alt, qual, filt, allele_depth_base, orig_pos])
            d.setdefault(alt, []).append(orig_pos)
        elif ((int(pos) == pos_old) and (orig_pos != orig_pos_old)):
            if (id == id_old):
                if flag == 2 and d != dict():
                    d = {x: [i for i in d[x] if not ''.join(id) in i] for x in d}
                    d = {k: v for k, v in d.items() if v}
                    if d == dict():
                        Collapsed_insertions.pop()
                        # dictionary is empty, record has been already inserted in Collapsed_insertions
                        # remove to avoid duplicate

                    for index, (key, value) in enumerate(d.items()):
                        alt_allele_counts = len(value)
                        allele_depth = "AD=" + ",".join([str(n_sequences - alt_allele_counts), str(alt_allele_counts)])
                        if index == 0:
                            Collapsed_insertions[-1][4] = key
                            Collapsed_insertions[-1][7] = allele_depth
                            Collapsed_insertions[-1][8] = ",".join(value)
                        else:
                            Collapsed_insertions.append([scaffold, str(pos_old), ID, ref, key, qual, filt, allele_depth, ",".join(value)])

                    d = dict()
                    Collapsed_insertions.append(line_old.rstrip("\n\r\b").split("\t"))

                flag = 1
                # same position, same gisaid id, long insertion
                Tmp_insertions.append(alt)

            else:
                if flag == 1:
                    # coming from previous long insertion
                    Collapsed_insertions[-1][4] = Collapsed_insertions[-1][4] + "".join(Tmp_insertions)
                    Collapsed_insertions[-1][7] = allele_depth_base
                    Collapsed_insertions.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])
                    Tmp_insertions = []
                    flag = 0
                else:
                    flag = 2
                    # same position, different gisaid id, new insertion
                    d.setdefault(alt, []).append(orig_pos)

        elif ((int(pos) == pos_old) and (orig_pos == orig_pos_old)):
            # duplicate insertion record
            continue

        else:
            if (flag == 2):
                for index, (key, value) in enumerate(d.items()):
                    alt_allele_counts = len(value)
                    allele_depth = "AD=" + ",".join([str(n_sequences - alt_allele_counts), str(alt_allele_counts)])
                    if index == 0:
                        Collapsed_insertions[-1][4] = key
                        Collapsed_insertions[-1][7] = allele_depth
                        Collapsed_insertions[-1][8] = ",".join(value)
                    else:
                        Collapsed_insertions.append([scaffold, str(pos_old), ID, ref, key, qual, filt, allele_depth, ",".join(value)])

            if (flag == 1):
                Collapsed_insertions[-1][4] = Collapsed_insertions[-1][4] + "".join(Tmp_insertions)
                Collapsed_insertions[-1][7] = allele_depth_base

            d = dict()
            Tmp_insertions = []
            Collapsed_insertions.append([scaffold, pos, ID, ref, alt, qual, filt, allele_depth_base, orig_pos])
            d.setdefault(alt, []).append(orig_pos)
        pos_old = int(pos)
        orig_pos_old = orig_pos
        id_old = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos_old.split("|")))
        line_old = line

        # last line
        if i == len(Insertions) - 1:
            if (flag == 2):
                for index, (key, value) in enumerate(d.items()):
                    alt_allele_counts = len(value)
                    allele_depth = "AD=" + ",".join([str(n_sequences - alt_allele_counts), str(alt_allele_counts)])
                    if index == 0:
                        Collapsed_insertions[-1][4] = key
                        Collapsed_insertions[-1][7] = allele_depth
                        Collapsed_insertions[-1][8] = ",".join(value)
                    else:
                        Collapsed_insertions.append([scaffold, pos, ID, ref, key, qual, filt, allele_depth, ",".join(value)])

            else:
                Collapsed_insertions[-1][4] = Collapsed_insertions[-1][4] + "".join(Tmp_insertions)
                Collapsed_insertions[-1][7] = allele_depth_base
                if Tmp_insertions == []:
                    Collapsed_insertions[-1][7] = allele_depth_base

    return Collapsed_insertions


def collapse_deletions(Deletions, n_sequences):
    Collapsed_deletions = []
    allele_depth_base = "AD=" + ",".join([str(n_sequences - 1), str(1)])
    for i in range(len(Deletions)):
        line = Deletions[i]
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        id = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos.split("|")))

        if len(Collapsed_deletions) == 0:
            Collapsed_deletions.append([scaffold, pos, ID, ref, alt, qual, filt, allele_depth_base, orig_pos])
        elif ((int(pos) == pos_old) and ((orig_pos != orig_pos_old) or (orig_pos == orig_pos_old))):
            if (id == id_old):
                # duplicate deletion record
                continue
            else:
                orig_pos_lst = str(Collapsed_deletions[-1][8]).split(",")
                orig_pos_lst.append(str(orig_pos))
                Collapsed_deletions[-1][8] = ",".join(orig_pos_lst)
                alt_allele_counts = len(orig_pos_lst)
                allele_depth = "AD=" + ",".join([str(n_sequences - alt_allele_counts), str(alt_allele_counts)])
                Collapsed_deletions[-1][7] = allele_depth

        elif ((int(pos) == pos_old + 1) and (id == id_old)):
            if len(str(Collapsed_deletions[-1][8]).split(",")) > 1 and ''.join(id) in str(Collapsed_deletions[-1][8]).split(",")[-1]:
                orig_pos_lst = str(Collapsed_deletions[-1][8]).split(",")
                orig_pos_lst.pop()
                Collapsed_deletions[-1][8] = ",".join(orig_pos_lst)
                alt_allele_counts = len(orig_pos_lst)
                allele_depth = "AD=" + ",".join([str(n_sequences - alt_allele_counts), str(alt_allele_counts)])
                Collapsed_deletions[-1][7] = allele_depth
                Collapsed_deletions.append(line_old.rstrip("\n\r\b").split("\t"))

            # same gisaid id, long deletion
            Collapsed_deletions[-1][3] = Collapsed_deletions[-1][3] + "".join(ref)

        else:
            Collapsed_deletions.append([scaffold, pos, ID, ref, alt, qual, filt, allele_depth_base, orig_pos])

        pos_old = int(pos)
        orig_pos_old = orig_pos
        line_old = line
        id_old = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos_old.split("|")))

    return Collapsed_deletions


def add_indel_first_base(Collapsed_indels, Sequences):
    Indels_w_first_base = []
    Joined_indels = ["\t".join(x) + "\n" for x in Collapsed_indels]
    for line in Joined_indels:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        # double -1, one is to make it 0-based, one is to take the position before the listed one
        ref_position = str(Sequences[scaffold][int(pos) - 1 - 1])
        if ref == ".":
            ref = ref_position
            alt = ",".join([str(ref_position) + str(indel) for indel in alt.split(",")])
        elif alt == ".":
            ref = ref_position + ref
            alt = ref_position

        # scaling pos within the file to the position before according to the guidelines
        pos = str(int(pos) - 1)
        Indels_w_first_base.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])

    return Indels_w_first_base


def count_indels(Collapsed_indels, n_sequences ):
    Indels_count = []
    df = DataFrame(Collapsed_indels, columns=['scaffold', 'pos', 'ID', 'ref', 'alt', 'qual', 'filt', 'var_type', 'orig_pos'])
    df.sort_values(['pos', 'ref', 'alt'], inplace=True)
    records = df.to_records(index=False)
    Collapsed_indels = list(records)
    Joined_indels = ["\t".join(x) + "\n" for x in Collapsed_indels]
    for line in Joined_indels:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        id = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos.split("|")))
        count = len(orig_pos.split(','))
        if len(Indels_count) == 0:
            Indels_count.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])
        elif ((int(pos) == pos_old) and (ref == ref_old) and (alt == alt_old) and (id != id_old)):
            # indel is repeated in another sequence
            count += count_old
            allele_depth = "AD=" + ",".join([str(n_sequences - count), str(count)])
            Indels_count[-1][7] = allele_depth
            Indels_count[-1][8] = Indels_count[-1][8] + "," + "".join(orig_pos)
        elif ((int(pos) == pos_old) and (ref == ref_old) and (alt == alt_old) and (id == id_old)):
            #duplicate record
            continue
        else:
            Indels_count.append([scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos])

        pos_old = int(pos)
        orig_pos_old = orig_pos
        ref_old = ref
        alt_old = alt
        id_old = list(filter(lambda x: 'EPI_ISL_' in x, orig_pos_old.split("|")))
        count_old = count

    return Indels_count


def parse_sequences(Reference):
    Sequences = {}
    for record in SeqIO.parse(Reference, "fasta"):
        name = str(record.id)
        seq = str(record.seq)
        Sequences[name] = seq

    return Sequences


def separate_vars_by_type(Vcf_lines):
    Snps = []
    Insertions = []
    Deletions = []
    for line in Vcf_lines:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        if var_type == ".":
            Snps.append(line)
        elif var_type == "INDEL":
            # insertion
            if ref == ".":
                Insertions.append(line)
            # deletion
            elif alt == ".":
                Deletions.append(line)


    return (Snps, Insertions, Deletions)


def collapse_variants(Reference, Vcf_lines):
    Sequences = parse_sequences(Reference)
    (Snps, Insertions, Deletions) = separate_vars_by_type(Vcf_lines)

    ### snps ###
    Sorted_snps = []
    for line in Snps:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        pos = int(pos)
        Sorted_snps.append((scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos))

    Sorted_snps = ["\t".join([str(y) for y in x]) for x in sorted(Sorted_snps, key=itemgetter(0, 1))]
    Collapsed_snps = collapse_snps(Sorted_snps, n_sequences)

    ### insertions ###
    Insertion_tuples = []
    for line in Insertions:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        pos = int(pos)
        Insertion_tuples.append((scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos))

    df = DataFrame(Insertion_tuples, columns=['scaffold', 'pos', 'ID', 'ref', 'alt', 'qual', 'filt', 'var_type', 'orig_pos'])
    df.sort_values(['orig_pos', 'pos'], inplace=True)
    records = df.to_records(index=False)
    Insertion_tuples = list(records)
    Insertions = ["\t".join([str(y) for y in x]) for x in Insertion_tuples]
    Collapsed_insertions = collapse_insertions(Insertions, n_sequences)

    ### deletions ###
    Deletion_tuples = []
    for line in Deletions:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")
        pos = int(pos)
        Deletion_tuples.append((scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos))

    df = DataFrame(Deletion_tuples, columns=['scaffold', 'pos', 'ID', 'ref', 'alt', 'qual', 'filt', 'var_type', 'orig_pos'])
    df.sort_values(['orig_pos', 'pos'], inplace=True)
    records = df.to_records(index=False)
    Deletion_tuples = list(records)
    Deletions = ["\t".join([str(y) for y in x]) for x in Deletion_tuples]
    Collapsed_deletions = collapse_deletions(Deletions, n_sequences)

    Collapsed_indels = Collapsed_insertions + Collapsed_deletions
    ### add prior base to indels ###
    Collapsed_indels = add_indel_first_base(Collapsed_indels, Sequences)
    Collapsed_indels = count_indels(Collapsed_indels, n_sequences)

    Vcf_lines = []
    for x in Collapsed_snps:
        Vcf_lines.append(x)
    for y in Collapsed_indels:
        Vcf_lines.append(y)

    ### sort ###
    Vcf_lines = sorted(Vcf_lines, key=itemgetter(1))

    ### return ###
    Vcf_lines = ["\t".join([str(y) for y in x]) for x in Vcf_lines]
    return Vcf_lines


def select_by_type(Vcf_lines, selected_type):
    Selected_vcf_lines = []
    for line in Vcf_lines:
        (scaffold, pos, ID, ref, alt, qual, filt, var_type, orig_pos) = line.rstrip("\n\r\b").split("\t")

        if ((selected_type == "ALL") or \
                (selected_type == "SNP" and var_type != "INDEL") or \
                (selected_type == "INDEL" and var_type == "INDEL")):
            Selected_vcf_lines.append(line)

    return Selected_vcf_lines


def sort_vcf_lines(Vcf_lines):
    Lines = [x.rstrip("\n\r\b").split("\t") for x in Vcf_lines]
    Lines_w_int = []
    for lst in Lines:
        lst[1] = int(lst[1])
        Lines_w_int.append(lst)

    Sorted_lines = sorted(Lines_w_int, key=itemgetter(0, 1))
    Vcf_lines = []
    for lst in Sorted_lines:
        lst[1] = str(lst[1])
        line = "\t".join(lst)
        Vcf_lines.append(line)

    return Vcf_lines


def add_header(Reference, Vcf_lines):
    file_format = "##fileformat=VCFv4.2"
    file_date = "##fileDate={0}".format(strftime("%Y%m%d"))
    source = "##source=my-mummer-2-vcf.py"
    reference_file = "##reference={0}".format(str(Reference))
    Header = [file_format, file_date, source, reference_file]

    Contigs_in_vars = list(set([line.rstrip("\n\r\b").split("\t")[0] for line in Vcf_lines]))
    Contigs = []
    for record in SeqIO.parse(Reference, "fasta"):
        ID = str(record.id)
        if ID in Contigs_in_vars:
            Length = str(len(str(record.seq)))
            Contigs.append("##contig=<ID={0},length={1}>".format(ID, Length))

    if len(Contigs) > 0:
        contig_lines = "\n".join(Contigs)
        Header.append(contig_lines)

    indel_line = '##INFO=<ID=INDEL,Number=0,Type=Flag,Description="Indicates that the variant is an INDEL.">'
    Header.append(indel_line)

    allele_depth_line = '##INFO=<ID=AD,Number=R,Type=Integer,Description="Total allelic depths">'
    Header.append(allele_depth_line)

    final_line = "\t".join(["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"])
    Header.append(final_line)
    Header = "\n".join(Header)
    Final_lines = [Header]
    for line in Vcf_lines:
        Final_lines.append(line)

    return Final_lines


# main script
if __name__ == "__main__":
    infile = args.snps
    INPUT = open(infile, "r")
    Lines = [line for line in INPUT]
    INPUT.close()

    if args.input_header:
        Lines = remove_header(Lines)

    Vcf_lines = [convert_snps_to_vcf(line) for line in Lines]

    if args.no_Ns:
        Vcf_lines = exclude_Ns(Vcf_lines)

    Vcf_lines = infer_var_type(Vcf_lines)
    Vcf_lines = select_by_type(Vcf_lines, args.type)
    n_sequences = count_sequences(Vcf_lines)
    Vcf_lines = collapse_variants(args.reference, Vcf_lines)
    Vcf_lines = sort_vcf_lines(Vcf_lines)

    if args.output_header:
        Vcf_lines = add_header(args.reference, Vcf_lines)

    for line in Vcf_lines:
        sys.stdout.write(line + "\n")
