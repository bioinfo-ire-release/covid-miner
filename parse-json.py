
# parsing GISAID json to produce FASTA
# we include submission date instead of collection date since there are many sequences with only the year "e.g. 2020" in the collection date  

import sys, json;

data = []

ff=open("gisaid.fasta", "w")

with open('gisaid.json') as f:
    for line in f:
        data.append(json.loads(line))
        ll=json.loads(line)
        name=ll["covv_virus_name"]
        sequence=ll["sequence"]
        date=ll["covv_subm_date"]
        accession_id=ll["covv_accession_id"]
        print(date)
        ff.write(">"+name+"|"+accession_id+"|"+date+"\n")
        ff.write(sequence+"\n")

ff.close
f.close 
