#!/bin/bash
set -e

if [[ -f ${1} ]];
then
    ${MAIN_FOLDER}/${MAIN_SCRIPT} $@
else
    $@
fi



